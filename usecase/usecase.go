package usecase

import (
	"tugasnya-tugas/models"
	"tugasnya-tugas/repo"
)

type UserUsecaseStruct struct {
	userUsecase repo.UserRepoInterface
}

type UserUsecaseInterface interface {
	AddUser(data *models.User) (*models.User, error)
}

func CreateUserUsecaseImpl(userUsecase repo.UserRepoInterface) UserUsecaseInterface {
	return &UserUsecaseStruct{userUsecase}
}

func (u *UserUsecaseStruct) AddUser(data *models.User) (*models.User, error) {
	res, _ := u.userUsecase.AddUser(data)
	return res, nil
}
