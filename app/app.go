package app

import (
	g "github.com/incubus8/go/pkg/gin"
	"github.com/rs/zerolog/log"
	"github.com/subosito/gotenv"
	"tugasnya-tugas/config"
	"tugasnya-tugas/router"
)

func init(){
	gotenv.Load()
}

func StartApp() {
	//Call property config in folder config
	addr := config.Config.ServiceHost + ":" + config.Config.ServicePort
	conf := g.Config{
		ListenAddr: addr,
		// function Router assign to property Handler
		Handler:    router.Router(),
		OnStarting: func() {
			log.Info().Msg("Your service is up and running at " + addr)
		},
	}

	//Service is up and running
	g.Run(conf)
}
