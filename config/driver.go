package config

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"tugasnya-tugas/models"
)

var DB *gorm.DB

func ConnectDB() *gorm.DB {

	if DB != nil {
		return DB
	}

	var err error
	dbConfig := Config.DB

	if dbConfig.Adapter == "mysql" {
		//DB, err = gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",dbConfig.UserDB,dbConfig.Password,dbConfig.Host,dbConfig.Port,dbConfig.Name))
		log.Println("Connected to Database Development")
	} else if dbConfig.Adapter == "postgres" {
		dsn := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", dbConfig.UserDB, dbConfig.Password, dbConfig.Host, dbConfig.Name)
		db, errs := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		err = errs
		DB = db
		log.Println("Connected to Database Local postgressql")
	}

	if err != nil {
		log.Println( "[Driver.ConnectDB] error when connect to database")
		log.Fatal(err)
	} else {
		log.Println( "SUCCES CONNECT TO DATABASE")
	}

	models.Migrate(DB)

	return DB
}
