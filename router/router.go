package router

import (
	"github.com/gin-gonic/gin"
	"tugasnya-tugas/config"
	"tugasnya-tugas/controller"
	"tugasnya-tugas/repo"
	"tugasnya-tugas/usecase"
)

func Router() *gin.Engine {
	router := gin.New()

	db := config.ConnectDB()

	userRepo := repo.CreateUsrRepoImpl(db)
	userUsecase := usecase.CreateUserUsecaseImpl(userRepo)

	v1 := router.Group("api/v1")

	{
		newRoute := v1.Group("belajar")
		controller.CreateUserControllerImpl(newRoute,userUsecase)
	}

	return router
}
