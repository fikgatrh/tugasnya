module tugasnya-tugas

go 1.16

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/gin-gonic/gin v1.7.1 // indirect
	github.com/incubus8/go v0.0.0-20180624150828-6dfffa306ab7
	github.com/joho/godotenv v1.3.0
	github.com/rs/zerolog v1.21.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/subosito/gotenv v1.2.0
	github.com/tylerb/graceful v1.2.15 // indirect
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.7
)
