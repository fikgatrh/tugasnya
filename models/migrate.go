package models

import "gorm.io/gorm"

func Migrate(db *gorm.DB) {
	_ = db.Debug().AutoMigrate(&User{})
}
