package repo

import (
	"gorm.io/gorm"
	"tugasnya-tugas/models"
)

type UserRepo struct {
	db *gorm.DB
}

type UserRepoInterface interface {
	AddUser(data *models.User) (*models.User, error)
}

func CreateUsrRepoImpl(db *gorm.DB) UserRepoInterface {
	return &UserRepo{db}
}

func (u *UserRepo) AddUser(data *models.User) (*models.User, error) {
	err := u.db.Debug().Create(&data).Error
	if err != nil {
		return nil, err
	}

	return data, nil
}
