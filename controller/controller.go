package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"tugasnya-tugas/models"
	"tugasnya-tugas/usecase"
)

type UserControllerStruct struct {
	userController usecase.UserUsecaseInterface
}

func CreateUserControllerImpl(router *gin.RouterGroup, userController usecase.UserUsecaseInterface) {
	inDB :=UserControllerStruct{userController}

	router.POST("/user", inDB.AddUser)
}

func (u *UserControllerStruct) AddUser(c *gin.Context) {
	var data models.User

	err := c.ShouldBindJSON(&data)
	if err != nil {

	}

	res, err := u.userController.AddUser(&data)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Oppss, something went erong")
	}

	c.JSON(http.StatusOK, res)
}
